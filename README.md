# Shopping List Python

# Install
    pip install -r requirements.txt

# Run
    python api.py

# References
    https://www.codingforentrepreneurs.com/blog/install-django-on-mac-or-linux/
    https://docs.sqlalchemy.org/en/latest/orm/tutorial.html 
    https://httpstatuses.com/ 

# Use VirtualEnvironment
