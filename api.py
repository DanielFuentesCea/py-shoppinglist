from flask import Flask, jsonify, request, json, abort
from flask_sqlalchemy import SQLAlchemy
from flask_marshmallow import Marshmallow


app = Flask(__name__)
app.config['SQLALCHEMY_DATABASE_URI'] = 'sqlite:////tmp/shopping.db'

db = SQLAlchemy(app)
ma = Marshmallow(app)

def today():
    return datetime.datetime.today().strftime('%Y-%m-%d')

class Item(db.Model):
    __tablename__ = 'item'
    id = db.Column(db.Integer, primary_key=True)
    name = db.Column(db.String(100), nullable=False)
    quantity = db.Column(db.Integer)
    shoppinglist_id = db.Column(db.Integer, db.ForeignKey('shoppinglist.id'), nullable=False)

    def __repr__(self):
        return self.name

class ItemSchema(ma.ModelSchema):
    class Meta:
        model = Item

class ShoppingList(db.Model):
    __tablename__ = 'shoppinglist'
    id = db.Column(db.Integer, primary_key=True)
    title = db.Column(db.String(100), unique=True, nullable=False)
    storeName = db.Column(db.String(100), nullable=False)
    date = db.Column(db.Date, nullable=True)
    items = db.relationship("Item")

    def __repr__(self):
        return self.title

class ShoppingListSchema(ma.ModelSchema):
    class Meta:
        model = ShoppingList


@app.route("/")
def server_info():

    return jsonify({
        "server": "My API"
    })

    
@app.route("/shopping", methods=['POST','GET', 'PUT', 'DELETE'])
def shopping():
    if request.method == 'POST': 

        values = request.get_json()
        shoppList = ShoppingList(
            title = values["title"], 
            storeName = values["storeName"]
        )

        db.session.add(shoppList)
        db.session.commit()

        return jsonify(
            status = "OK"
        ), 201

    if request.method == 'GET':

        shoppingList = ShoppingList.query.all()
        shoppingListSchema = ShoppingListSchema(many=True)
        output = shoppingListSchema.dump(shoppingList).data
    
        return jsonify({'shoppingList': output})


    if request.method == 'PUT': 
        values = request.get_json()
        
        shoppingList = ShoppingList.query.filter_by(id = values['id']).first()

        if shoppingList is None:
            return jsonify(
                status = "No Content"
            ), 204

        shoppingList.title = values['title']
        shoppingList.storeName = values['storeName']

        db.session.commit()

        return jsonify(
            status = "Updated"
        )

    if request.method == 'DELETE':
        values = request.get_json()
    
        shoppingList = ShoppingList.query.filter_by(id = values['id']).first()

        if shoppingList is None:
            return jsonify(
                status = "No Content"
            ), 204

        db.session.delete(shoppingList)
        db.session.commit()

        return jsonify(
            status = "Deleted"
        )



@app.route("/item", methods=['POST','GET'])
def addItem():

    if request.method == 'POST':
        values = request.get_json()

        for i in values:

            exist = Item.query.filter_by(name = i['name']).filter_by(shoppinglist_id = i['shoppinglist_id']
                ).first()

            if exist is None:
                item = Item(
                    name = i['name'],
                    quantity = i['quantity'],
                    shoppinglist_id = i['shoppinglist_id']
                )
                db.session.add(item)
                db.session.commit()

            else:

                exist.quantity = exist.quantity + i['quantity']
                db.session.commit()
                
        items = Item.query.filter_by(shoppinglist_id = exist.shoppinglist_id)
        itemSchema = ItemSchema(many=True)
        output = itemSchema.dump(items).data

        return jsonify({'ShoppingList': output})

    if request.method == 'GET':
        
        # items = db.session.query(Item).join(ShoppingList, Item.shoppinglist_id == ShoppingList.id)
        items = Item.query.all()

        if items is None:
            abort(404)
        itemSchema = ItemSchema(many=True)
        output = itemSchema.dump(items).data
    
        return jsonify({'Items': output})

@app.route("/shopping/perTitle", methods=['GET'])
def shoppingPerTitle():

    if request.method == 'GET':

        title = request.args.get('title')

        # shoppList = ShoppingList.query.filter_by(title=title).first()
        # shoppList = ShoppingList.title.like('%' + title + '%')
        # shopping = ShoppingListSchema.query.filter_by(title = title).first()
        shoppList = ShoppingList.query.filter(ShoppingList.title.like("%" + title + "%")).all()


        shoppingListSchema = ShoppingListSchema(many=True)
        output = shoppingListSchema.dump(shoppList).data

        return jsonify({'ShoppingListPerTitle': output})


if __name__ == "__main__":
    db.create_all() 
    app.run(debug=True, port=5000, host="0.0.0.0")